<?php
  require_once('admin/phpscripts/config.php');
  // confirm_logged_in();
  if(isset($_POST['submit'])) {
    $fname = trim($_POST['fname']);
    $username = trim($_POST['username']);
    $password = trim($_POST['password']);
    $email = trim($_POST['email']);
    if($fname == "" && $username = "" && $password = "" && $email = ""){ // == not identical to
      $message = "Please fill all the fields";
    }else{
      $encript = criptpass($password);
      $result = createUser($fname, $username, $email, $password);
      $message= $result;
    }
  }
 ?>

 <!DOCTYPE html>
 <html lang="en">
 <head>
 	<meta charset="UTF-8">
 	<title>Organ Donation</title>
 	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
 	<link rel="stylesheet" type="text/css" href="css/foundation.min.css">
 	<link rel="stylesheet" type="text/css" href="css/main.css">
 </head>
 <body>
  <div class="container__login">
    <form action="reg.php" method="post" class="form">
      <div class="form__title">
        JOIN US FOR MORE NEWS
      </div>
      <p style="color: white;"><?php if (!empty($message)){echo $message; } ?></p>
      <?php
      $tbl = "_tbl_register";
      $lang = "en";
      $contents = generalCnt($tbl, $lang);

      while ($row = mysqli_fetch_array($contents)) {
        if($row['register_imput'] === "submit") {
          echo "<div class=\"form__action\">
          <input class=\"button\" type=\"{$row['register_imput']}\" name=\"{$row['register_imput']}\" value=\"{$row['register_label']}\">
        ";
        }else{
          echo "<label for=\"\">{$row['register_label']}</label>
          <input type=\"{$row['register_type']}\" name=\"{$row['register_imput']}\" value=\"\">";
        }
      }
       ?>
       <div class="message">
         <span>Already have an account?</span>
         <br>
         <a href="userlogin.php" class="form__link">Sign In Here</a>
       </div>
     </div>
     <br>

    </form>
    <div class="form__footer">
			By clicking "Sign up". you agree to our <a href="">terms of service</a> and <a href="#">privacy policy.</a> We'll occasionally send you account related emails
		</div>
		<br>
	</div>

</body>
</html>
