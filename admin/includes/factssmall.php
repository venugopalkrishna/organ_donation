<?php
$tbl = "_tbl_fact";
$contents = generalCnt($tbl, $lang);

while ($row = mysqli_fetch_array($contents)) {
  echo "<div class=\"data cell medium-2\">
   				<div class=\"data__title\">
          {$row['fact_first']}
   				</div>
   				<div class=\"data__subtitle\">
          {$row['fact_second']}
   				</div>
   				<div class=\"data__description\">
          {$row['fact_third']}
   				</div>
   				<div class=\"data__icon\">
          <img src=\"img/{$row['fact_icon']}\" alt=\"{$row['fact_icon']}\">
   				</div>
   			</div>";
}

 ?>
