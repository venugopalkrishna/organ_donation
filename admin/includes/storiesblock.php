<?php
require_once('admin/phpscripts/config.php');
// if(isset($_GET['received'])){
$tbl = "_tbl_stories";
// $lang = "en";
$contents = generalCnt($tbl, $lang);

while ($row = mysqli_fetch_array($contents)) {
  $classes =  ["accordion-one", "accordion-two", "accordion-three"];
  $id = $row['stories_id'];
  $count = ($id - 1);
  // echo ($classes[1]);
  echo "<div class=\"storie medium-4\" onclick=\"openTab('{$classes[$count]}');\">
    <img class=\"storie__image\" src=\"img/{$row['stories_img']}\" alt=\"{$row['stories_img']}\">
    <div class=\"storie__description\">
      <h3>{$row['stories_bigtext']}</h3>
      <p>{$row['stories_smalltext']}</p>
    </div>
  </div>";
    }
?>
