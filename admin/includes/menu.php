<header>
  <div class="title-bar topbar-center-logo-mobile" data-responsive-toggle="topbar-center-logo" data-hide-for="medium">
      <div class="title-bar-left">
        <button class="menu-icon" type="button" data-toggle="topbar-center-logo"></button>
        <img src="img/search_bar.png" alt="">
      </div>

    <div class="title-bar-center">
      <div class="title-bar-title"><img src="img/logo_temp.png" alt="" /></div>
    </div>

    <div class="title-bar-right">
      <img src="img/sign_In.png" alt="">
    </div>


  </div>
  <!-- /mobile nav bar -->

  <!-- medium and larger nav bar -->
  <div class="top-bar topbar-center-logo" id="topbar-center-logo">
      <div class="top-bar-left">
        <ul class="menu vertical medium-horizontal" data-magellan>
          <li class="icon__item"><img src="img/search_bar.png" alt=""></li>
          <li><a href="#about">ABOUT</a></li>
          <li><a href="#stories">STORIES</a></li>
        </ul>
      </div>

      <div class="top-bar-center">
        <img src="img/logo_temp.png" alt="">
      </div>

    <div class="top-bar-right">
      <ul class="menu vertical medium-horizontal">
        <li><a href="#share">SHARE</a></li>
        <li><a href="#contact">CONTACT</a></li>
        <li class="icon__item">EN <div><img src="img/sign_In.png" alt=""></div></li>
      </ul>
    </div>
  </div>
</header>
