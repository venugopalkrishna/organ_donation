<?php
require_once('phpscripts/config.php');
$ip = $_SERVER['REMOTE_ADDR'];// if asked, you can make sure that the only place where you can access the info is IN THE OFFICE

if(isset($_POST['submit'])){
	$useremail = $_POST['useremail'];
	$password = trim($_POST['password']);
	if($useremail !== "" && $password !== ""){ // == not identical to
		$result = logIn($useremail, $password, $ip);
		$message = $result;
	}else{
		$message = "Please fill out the required (ALL) fields";
	}
}
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Organ Donation</title>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link rel="stylesheet" type="text/css" href="../css/foundation.min.css">
	<link rel="stylesheet" type="text/css" href="../css/main.css">
</head>
<body>

	<div class="container__login">
		<?php if(!empty($message)){ echo $message; } ?>
		<form action="admin_login.php" method="post" class="form">
			<div class="form__title">
				JOIN US FOR MORE NEWS
			</div>

			<label for="">Username or E-mail</label>
			<input type="text" name="useremail" value="">

			<label for="">Password</label>
			<input type="password" name="password" value="">

			<div class="form__link">Forgot your password?</div>

			<div class="form__action">
				<input class="button" type="submit" name="submit" value="SIGN IN">
				<div class="message">
					<span>New to Celebr8Lives?</span>
					<br>
					<a href="reg.php" class="form__link">Sign Up Here</a>
				</div>
			</div>

		</form>

		<div class="form__footer form__footer--down">
			<a href="#">team of services</a>
			<span>Privacy police</span>
			<a href="#">Security</a>
			<a href="index.php">Contact Us</a>
			<a href="https://www.beadonor.ca/">beadonor.ca</a>
		</div>
		<br>
	</div>


</body>
</html>
