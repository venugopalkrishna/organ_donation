<?php
function getSingle($lang, $tbl, $col, $id) {
  include('connect.php');
  $querySingle = "SELECT * FROM {$lang}_tbl_{$tbl} WHERE {$col} = {$id}";
  // echo $querySingle;
  $runSingle = mysqli_query($link, $querySingle);

  if($runSingle){
    return $runSingle;
  }else{
    $error = "Something has gone wrong, please come back later.";
    return $error;
  }

  mysqli_close($link);
}

  function single_edit($lang, $tbl, $col, $id) {

    $result = getSingle($lang, $tbl, $col, $id);
    $getResult = mysqli_fetch_array($result);

    echo "<form action=\"phpscripts/edit.php\" method=\"post\" enctype=\"multipart/form-data\">";

    echo "<input hidden name=\"tbl\" value=\"{$tbl}\">";
    echo "<input hidden name=\"lang\" value=\"{$lang}\">";
    echo "<input hidden name=\"col\" value=\"{$col}\">";
    echo "<input hidden name=\"id\" value=\"{$id}\">";

    // echo mysqli_num_fields($result);
    for($i=0; $i<mysqli_num_fields($result); $i++) {
      $dataType = mysqli_fetch_field_direct($result, $i);
      $fieldName = $dataType->name; // kinda like something inside of an array
      $fieldType = $dataType->type;

      if($fieldName != $col){
        echo "<label>{$fieldName}</label><br>";

        if($fieldName == $tbl."_img"){
          echo "<img style=\"width: 250px;\" src=\"../img/{$getResult[$i]}\">";
          echo "<input type=\"file\" name=\"{$fieldName}\" value=\"{$getResult[$i]}\"><br><br>";
        }elseif($fieldName == $tbl."_icon"){
          echo "<input hidden name=\"{$fieldName}\" value=\"{$getResult[$i]}\"><br><br>";
      }elseif($fieldType != "252"){
          echo "<input type=\"text\" name=\"{$fieldName}\" value=\"{$getResult[$i]}\"><br><br>";
        }else {
          echo "<textarea name=\"{$fieldName}\">{$getResult[$i]}</textarea><br><br>";
        }
      }

      // echo $fieldName."<br>";
      // echo $fieldType."<br>";
    }
    echo "<input class=\"button\" type=\"submit\" name=\"submit\" value=\"Save Content\">";
		echo "</form><br><br>";
  }

  function addmore($lang, $stbl, $tbl, $col) {

    $result = generalCnt($stbl, $lang);
    $getResult = mysqli_fetch_array($result);

    echo "<form action=\"phpscripts/adding.php\" method=\"post\" enctype=\"multipart/form-data\">";

    echo "<input hidden name=\"tbl\" value=\"{$tbl}\">";
    echo "<input hidden name=\"lang\" value=\"{$lang}\">";
    echo "<input hidden name=\"col\" value=\"{$col}\">";
    // echo "<input hidden name=\"id\" value=\"{$id}\">";

    // echo mysqli_num_fields($result);
    for($i=0; $i<mysqli_num_fields($result); $i++) {
      $dataType = mysqli_fetch_field_direct($result, $i);
      $fieldName = $dataType->name; // kinda like something inside of an array
      $fieldType = $dataType->type;

      if($fieldName != $col){
        echo "<label>{$fieldName}</label><br>";

        if($fieldName == $tbl."_img"){
          // echo "<img style=\"width: 250px;\" src=\"../img/{$getResult[$i]}\">";
          echo "<input type=\"file\" name=\"{$fieldName}\" value=\"\"><br><br>";
        }elseif($fieldName == $tbl."_icon"){
          echo "<input hidden name=\"{$fieldName}\" value=\"\"><br><br>";
      }elseif($fieldType != "252"){
          echo "<input type=\"text\" name=\"{$fieldName}\" value=\"\"><br><br>";
        }else {
          echo "<textarea name=\"{$fieldName}\"></textarea><br><br>";
        }
      }

      // echo $fieldName."<br>";
      // echo $fieldType."<br>";
    }
    echo "<input class=\"button\" type=\"submit\" name=\"toadd\" value=\"Save Content\">";
		echo "</form><br><br>";
  }

 ?>
