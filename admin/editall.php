<?php
require_once('phpscripts/config.php');
confirm_logged_in();

$lang = "en";
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Organ Donation</title>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link rel="stylesheet" type="text/css" href="../css/foundation.min.css">
	<link rel="stylesheet" type="text/css" href="../css/main.css">
</head>


<body>
  <div class="row" style="margin-top: 1em; padding: 2em;">
    <div class="columns">
      <a style="float: right;" href="phpscripts/caller.php?caller_id=logout">Sign out</a>
      <h2>WELCOME TO YOUR CMS PANEL</h2>
      <p>please select the option you want to edit</p>
    </div>
  </div>
  <div class="" style="padding-left: 20%; padding-right: 20%;">
    <?php
      if(isset($_POST['submit'])){
        $tbl = $_POST['tbl'];
        $col = $_POST['col'];
        $id = $_POST['id'];
        // $img = $_FILE[$tbl.'_img'];
        echo "<h1 class=\"sectionTitle\">Edit</h1>";
        $redult = single_edit($lang, $tbl, $col, $id);
        // $imgadded = addimg($img);
      }
    ?>
  </div>
  <div class="" style="padding-left: 20%; padding-right: 20%;">
    <?php
      if(isset($_POST['toadd'])){
        $tbl = $_POST['tbl'];
        $col = $_POST['col'];
        $stbl = "_tbl_".$tbl;
        // $id = $_POST['id'];
        // $img = $_FILE[$tbl.'_img'];
        echo "<h1 class=\"sectionTitle\">Add content</h1>";
        $result = addmore($lang, $stbl, $tbl, $col);
        // $imgadded = addimg($img);
      }
    ?>
  </div>


  <ul class="tabs" data-tabs id="option-tabs">
    <li class="tabs-title is-active"><a href="#panel1" aria-selected="true">ABOUT</a></li>
    <li class="tabs-title"><a data-tabs-target="panel2" href="#panel2">STORY</a></li>
    <!-- <li class="tabs-title"><a data-tabs-target="panel3" href="#panel3">SHARE</a></li>
    <li class="tabs-title"><a data-tabs-target="panel4" href="#panel4">X</a></li> -->
  </ul>


  <div class="tabs-content" data-tabs-content="option-tabs">
    <!--ABOUT-->
    <div class="tabs-panel is-active" id="panel1">

<!--facts start-->
     <h1 class="sectionTitle">FACTS</h1>

        <div id="facts">
          <?php
          $tbla = "_tbl_fact";
          // $lang = "en";
          $contents = generalCnt($tbla, $lang);
          while ($row = mysqli_fetch_array($contents)) {
            echo "<form class=\"factImg\" style=\"display: inline-block;\" action=\"editall.php\" method=\"post\">
                <img src=\"../img/{$row['fact_icon']}\" alt=\"{$row['fact_icon']}\"><br><br>
                <input hidden type=\"hidden\" name=\"tbl\" value=\"fact\">
                <input hidden type=\"hidden\" name=\"col\" value=\"fact_id\">
                <input hidden type=\"hidden\" name=\"id\" value=\"{$row['fact_id']}\">
                <input class=\"button\" type=\"submit\" name=\"submit\" value=\"EDIT\">
              </form>";
             }
           ?>
        </div><br><br>
<!--facts end-->

<!-- register start -->

  <h2 class="sectionTitle">REGISTER</h1>

<div id="register">
  <div class="grid-x">

  <?php
  $tbla = "_tbl_about";
  // $lang = "en";
  $contents = generalCnt($tbla, $lang);
  while ($row = mysqli_fetch_array($contents)) {
    echo "<div class=\"cell medium-4 registerDivs\">
    <form class=\"\" action=\"editall.php\" method=\"post\">";

    $id = $row['about_id'];
    if($id === "7"){
      echo "<div class=\"cell medium-4 registerDivs\">
     <button style=\"margim: 30px;\" class=\"button\">{$row['about_icon']}</button>
     </div>
     <h4 class=\"about__item__title\">{$row['about_title']}</h4>
     <p class=\"about__item__description\">{$row['about_text']}</p>";
    }else{
      echo "<img style=\"height: 60px;\" src=\"../img/{$row['about_icon']}\" class=\"about__item__img\" alt=\"{$row['about_img']}\">
     <h4 class=\"about__item__title\">{$row['about_title']}</h4>
     <p class=\"about__item__description2\">{$row['about_text']}</p>";
    }
     echo "<input hidden type=\"hidden\" name=\"tbl\" value=\"about\">
     <input hidden type=\"hidden\" name=\"col\" value=\"about_id\">
     <input hidden type=\"hidden\" name=\"id\" value=\"{$row['about_id']}\">
     <input class=\"button\" type=\"submit\" name=\"submit\" value=\"EDIT\">
   </form>
   </div>";
   }
   ?>
  </div>
 </div><br><br>
 <!-- register section end -->

 <!-- myth start -->

   <h1 class="sectionTitle">MYTH</h1>
   <form class="" action="editall.php" method="post">
     <input hidden type="hidden" name="tbl" value="myth">
     <input hidden type="hidden" name="col" value="myth_id">
     <input class="button" type="submit" name="toadd" value="ADD">
  </form>

 <div id="facts" class="medium-7">

   <?php
   $tbla = "_tbl_myth";
   // $lang = "en";
   $contents = generalCnt($tbla, $lang);
   while ($row = mysqli_fetch_array($contents)) {
     echo "<form class=\"\" action=\"editall.php\" method=\"post\">
         <h2>{$row['myth_title']}</h2>
         <input hidden type=\"hidden\" name=\"tbl\" value=\"myth\">
         <input hidden type=\"hidden\" name=\"col\" value=\"myth_id\">
         <input hidden type=\"hidden\" name=\"id\" value=\"{$row['myth_id']}\">
         <input class=\"button\" type=\"submit\" name=\"submit\" value=\"EDIT\">
       </form>";
      }
    ?>
 </div><br><br>

 <!-- myth end -->

</div><!--panel1 tab ends-->

    <!--STORY-->
    <div class="tabs-panel" id="panel2">
      <h1 class="sectionTitle">Story</h1>
      <div id="story" class="medium-7 edit">

      <?php
        $tbla = "_tbl_stories";
        // $lang = "en";
        $contents = generalCnt($tbla, $lang);
        while ($row = mysqli_fetch_array($contents)) {
          // $classes =  ["accordion-one", "accordion-two", "accordion-three"];
          $id = $row['stories_id'];
          $count = ($id - 1);
          // echo ($classes[1]);
          echo "<li style=\"display: inline-block;\"><form class=\"small-4 medium-4\" action=\"editall.php\" method=\"post\">
          <div class=\"storie small-3 medium-3\" onclick=\"openTab('accordion-one');\">
          <img style=\"width: 250px;\" class=\"storie__image\" src=\"../img/{$row['stories_img']}\" alt=\"{$row['stories_img']}\">
          </div>
          <input hidden type=\"hidden\" name=\"tbl\" value=\"stories\">
          <input hidden type=\"hidden\" name=\"col\" value=\"stories_id\">
          <input hidden type=\"hidden\" name=\"id\" value=\"{$row['stories_id']}\">
          <input class=\"button\" type=\"submit\" name=\"submit\" value=\"EDIT\">
          </form></li>";
        }
        ?>

    </div>

    <div class="">
      <h1 class="sectionTitle">Posted stories</h1>
      <?php
      $tbla = "_tbl_share";
      // $lang = "en";
      $contents = generalCnt($tbla, $lang);
        while ($row = mysqli_fetch_array($contents)) {
          echo "<form action=\"editall.php\" method=\"post\">
          <div class=\"is-active orbit-slide\">
              <div class=\"docs-example-orbit-slide\">
              <h3 class=\"title\">{$row['share_name']}, {$row['share_age']}, {$row['share_location']}</h3>
              <p>{$row['share_story']}</p>
              </div>
          </div>
          <input hidden type=\"hidden\" name=\"tbl\" value=\"share\">
          <input hidden type=\"hidden\" name=\"col\" value=\"share_id\">
          <input hidden type=\"hidden\" name=\"id\" value=\"{$row['share_id']}\">
          <input class=\"button\" type=\"submit\" name=\"submit\" value=\"EDIT\">
          </form>";
        }
       ?>
    </div>

    <!--SHARE-->
    <!-- <div class="tabs-panel" id="panel3">
      <p>SHARE</p>
    </div> -->

   <!--X-->
    <!-- <div class="tabs-panel" id="panel4">
      <p>x</p>
    </div> -->
  </div>

   <script src="../js/vendor/jquery.js"></script>
     <script src="../js/vendor/what-input.js"></script>
     <script src="../js/vendor/foundation.js"></script>
     <script src="../js/app.js"></script>
 	<script src="../js/principal.js"></script>
</body>
</html>
