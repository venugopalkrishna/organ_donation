-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 28-03-2018 a las 07:09:46
-- Versión del servidor: 5.7.19
-- Versión de PHP: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `db_organ`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `en_tbl_about`
--

DROP TABLE IF EXISTS `en_tbl_about`;
CREATE TABLE IF NOT EXISTS `en_tbl_about` (
  `about_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `about_title` varchar(400) NOT NULL,
  `about_text` text NOT NULL,
  `about_img` varchar(50) NOT NULL,
  PRIMARY KEY (`about_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `en_tbl_about`
--

INSERT INTO `en_tbl_about` (`about_id`, `about_title`, `about_text`, `about_img`) VALUES
(1, 'Your Decision', 'Becoming an organ donor is a personal choice, We are here to fel inspired to make one of the best choices of our lives.', 'decision-icon.png'),
(2, 'Your Health Card', 'A card that protects your health and takes you one step closer to saving others.', 'healthcardIcon.png'),
(3, 'This Button', 'Come to beadonor.ca and put action to your wish, learn more facts and be updated with community events', 'REGISTER NOW');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `en_tbl_contact`
--

DROP TABLE IF EXISTS `en_tbl_contact`;
CREATE TABLE IF NOT EXISTS `en_tbl_contact` (
  `contact_id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT,
  `contact_formtitle` varchar(70) NOT NULL,
  `contact_connecttitle` varchar(72) NOT NULL,
  `contact_hotline` varchar(70) NOT NULL,
  `contact_number` varchar(20) NOT NULL,
  PRIMARY KEY (`contact_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `en_tbl_contact`
--

INSERT INTO `en_tbl_contact` (`contact_id`, `contact_formtitle`, `contact_connecttitle`, `contact_hotline`, `contact_number`) VALUES
(1, 'how can we help?', 'connect with us', 'Our hotline', '1-888-888-888');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `en_tbl_facts`
--

DROP TABLE IF EXISTS `en_tbl_facts`;
CREATE TABLE IF NOT EXISTS `en_tbl_facts` (
  `fact_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT,
  `fact_first` varchar(50) NOT NULL,
  `fact_second` varchar(50) NOT NULL,
  `fact_third` varchar(50) NOT NULL,
  `fact_icon` varchar(40) NOT NULL,
  PRIMARY KEY (`fact_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `en_tbl_facts`
--

INSERT INTO `en_tbl_facts` (`fact_id`, `fact_first`, `fact_second`, `fact_third`, `fact_icon`) VALUES
(1, 'ONE DONOR', 'can save up to 8 lives', 'and enhance lives of 75 more', 'gift_icon.png'),
(2, '1,527', 'patients', 'waiting for organ transplant ', 'puzzle_icon.png'),
(3, '32%', 'registered', 'to be an organ donor ', 'heart_icon.png'),
(4, 'ALL AGES', 'may qualify', 'to be an organ donor', 'champagne_icon.png'),
(5, 'ALL MAJOR RELIGIONS', 'support', 'organ donation', 'check_icon.png');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `en_tbl_myth`
--

DROP TABLE IF EXISTS `en_tbl_myth`;
CREATE TABLE IF NOT EXISTS `en_tbl_myth` (
  `myth_id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT,
  `myth_title` varchar(250) NOT NULL,
  `myth_desc` text NOT NULL,
  PRIMARY KEY (`myth_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `en_tbl_myth`
--

INSERT INTO `en_tbl_myth` (`myth_id`, `myth_title`, `myth_desc`) VALUES
(1, 'MYTH #1 I’m too old to be an organ and tissue donor', 'Age is not a barrier - people over 80 have become organ and tissue donors\r\n\r\n- The majority (78%) of Australians aged 65+ years are willing to donate organs and tissues, yet 37% assume they are too old to be considered for organ and tissue donation.\r\n- People in their 70\'s and 80\'s have saved the lives of others through organ and tissue donation.\r\n- Each potential donor is assessed on an individual basis. There is every possibility you may be able to donate your organs or tissues.');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `en_tbl_promotional`
--

DROP TABLE IF EXISTS `en_tbl_promotional`;
CREATE TABLE IF NOT EXISTS `en_tbl_promotional` (
  `prom_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT,
  `prom_image` varchar(80) NOT NULL,
  `prom_video` varchar(80) NOT NULL,
  PRIMARY KEY (`prom_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `en_tbl_register`
--

DROP TABLE IF EXISTS `en_tbl_register`;
CREATE TABLE IF NOT EXISTS `en_tbl_register` (
  `register_id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT,
  `register_label` varchar(20) NOT NULL,
  `register_imput` varchar(20) NOT NULL,
  `register_type` varchar(20) NOT NULL,
  PRIMARY KEY (`register_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `en_tbl_register`
--

INSERT INTO `en_tbl_register` (`register_id`, `register_label`, `register_imput`, `register_type`) VALUES
(1, 'Username', 'username', 'text'),
(2, 'Fullname', 'fname', 'text'),
(3, 'Email', 'email', 'email'),
(4, 'Password', 'password', 'password'),
(5, 'SIGN UP', 'submit', 'submit');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `en_tbl_share`
--

DROP TABLE IF EXISTS `en_tbl_share`;
CREATE TABLE IF NOT EXISTS `en_tbl_share` (
  `share_id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT,
  `share_name` varchar(100) NOT NULL,
  `share_age` tinyint(15) NOT NULL,
  `share_location` varchar(150) NOT NULL,
  `share_type` varchar(30) NOT NULL,
  `share_title` varchar(20) DEFAULT NULL,
  `share_story` text NOT NULL,
  `share_picture` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`share_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `en_tbl_share`
--

INSERT INTO `en_tbl_share` (`share_id`, `share_name`, `share_age`, `share_location`, `share_type`, `share_title`, `share_story`, `share_picture`) VALUES
(1, 'RYLEY', 12, 'TORONTO', 'received', NULL, 'When Ryley was two months old she started to vomit and was making a little sighing noise when she was breathing. We thought she just had a \'bug\' but after a couple of days of this, we decided to have a doctor look at her. It was the evening and the ER was empty that night and I think that may have saved Ryley\'s life. The doctor examined her, did a chest x-ray and then told us they were going to transfer us to the closest children\'s hospital to do more tests. When we arrived there, our tiny little baby was swarmed by doctors and nurses trying to examine her and get blood. Through all the commotion, I remember seeing the one doctor standing in the background holding up the chest x-ray and saying \"enlarged heart\". I didn\'t really know what that meant, but I knew it was bad. Soon a cardiologist came and did an echo on Ryley and within an hour of arriving we had the diagnosis of Dilated Cardiomyopathy. When the heart becomes enlarged, it cannot effectively pump blood to the rest of your body and your other organs start to fill with fluid and shut down. With the severity of Ryley\'s enlargement, it was a miracle she was still alive. I remember asking the doctor if this meant she was going to die. He said no but she would likely be a candidate for a heart transplant. My child\'s first haircut was when they shaved a spot on her scalp because it was the only place they could get an IV in.', NULL),
(3, 'KAREN', 20, 'LONDON, ONTARIO', 'donor', NULL, 'I registered as an organ and tissue donor at Service Ontario when I renewed my health card. It means a lot to know that when my time\'s up there is a chance my organs could help save somebody\'s life. My tissues could improve many lives too, and that\'s really cool - I sure won\'t need them anymore! Also I\'ve learned that there\'s very rare chances to donate organs, only certain circumstances where someone dies at the hospital. So the more people registered the better.', 'stories2.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `en_tbl_stories`
--

DROP TABLE IF EXISTS `en_tbl_stories`;
CREATE TABLE IF NOT EXISTS `en_tbl_stories` (
  `stories_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT,
  `stories_bigtext` varchar(50) NOT NULL,
  `stories_smalltext` varchar(200) NOT NULL,
  `stories_img` varchar(30) NOT NULL,
  PRIMARY KEY (`stories_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `en_tbl_stories`
--

INSERT INTO `en_tbl_stories` (`stories_id`, `stories_bigtext`, `stories_smalltext`, `stories_img`) VALUES
(1, '\"WITHOUT THE TRANSPLANT', 'I might not see my family anymore\"', 'stories1.jpg'),
(2, '\"I CHOSE TO BE A DONOR', 'because i want to make sure life goes on.\"', 'stories2.jpg'),
(3, '\"WHAT\'S YOUR STORY?', 'share your inspiration with us.\"', 'stories3.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `en_tbl_storieshare`
--

DROP TABLE IF EXISTS `en_tbl_storieshare`;
CREATE TABLE IF NOT EXISTS `en_tbl_storieshare` (
  `ss_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ss_header` varchar(50) NOT NULL,
  `ss_name` varchar(50) NOT NULL,
  `ss_age` varchar(20) NOT NULL,
  `ss_location` varchar(50) NOT NULL,
  `ss_anon` varchar(70) NOT NULL,
  `ss_rec` varchar(40) NOT NULL,
  `ss_reg` varchar(40) NOT NULL,
  `ss_fam` varchar(80) NOT NULL,
  `ss_title` varchar(40) NOT NULL,
  `ss_message` varchar(40) NOT NULL,
  `ss_button` varchar(40) NOT NULL,
  PRIMARY KEY (`ss_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `en_tbl_storieshare`
--

INSERT INTO `en_tbl_storieshare` (`ss_id`, `ss_header`, `ss_name`, `ss_age`, `ss_location`, `ss_anon`, `ss_rec`, `ss_reg`, `ss_fam`, `ss_title`, `ss_message`, `ss_button`) VALUES
(1, 'Share your story, inspire others', 'Your name', 'Age', 'Location', 'Anonimous (Name will not be displayed)', 'Receiver', 'Registered', 'Family of Receiver or Donor', 'Title (optional)', 'Your message', 'SEND');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `en_tbl_titles`
--

DROP TABLE IF EXISTS `en_tbl_titles`;
CREATE TABLE IF NOT EXISTS `en_tbl_titles` (
  `titles_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `titles_name` varchar(50) NOT NULL,
  `titles_pagetitle` varchar(120) DEFAULT NULL,
  PRIMARY KEY (`titles_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `en_tbl_titles`
--

INSERT INTO `en_tbl_titles` (`titles_id`, `titles_name`, `titles_pagetitle`) VALUES
(1, 'ABOUT', NULL),
(2, 'STORIES', NULL),
(3, 'SHARE', 'SHARE your wish to be a donor. talk to your family today'),
(4, 'CONTACT', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `en_tbl_wish`
--

DROP TABLE IF EXISTS `en_tbl_wish`;
CREATE TABLE IF NOT EXISTS `en_tbl_wish` (
  `wish_id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT,
  `wish_email` varchar(80) NOT NULL,
  `wish_title` varchar(50) NOT NULL,
  `wish_message` text NOT NULL,
  `wish_head` varchar(120) NOT NULL,
  PRIMARY KEY (`wish_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fr_tbl_about`
--

DROP TABLE IF EXISTS `fr_tbl_about`;
CREATE TABLE IF NOT EXISTS `fr_tbl_about` (
  `about_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT,
  `about_title` varchar(200) NOT NULL,
  `about_text` text NOT NULL,
  PRIMARY KEY (`about_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `fr_tbl_about`
--

INSERT INTO `fr_tbl_about` (`about_id`, `about_title`, `about_text`) VALUES
(1, 'le processus de don d’organes et d’tissus', 'La première étape consiste à vous inscrire pour être un donneur d\'organes et de tissus et informer votre famille de cette décision. Pour vous inscrire et faire partie de la base de données confidentielle des donneurs, vous devez avoir une carte Santé de l\'Ontario valide. Vous pouvez inscrire à BeADonor.ca ou en personne à n\'importe quel endroit de ServiceOntario.  (Veuillez noter que les anciennes cartes de donneur ne sont plus valides.) Il est important de partager votre souhait d\'être un donneur d\'organes avec votre famille, car ils ont besoin de donner leur consentement du don quand le moment venu. Ce choix est un cadeau qui peut sauver jusqu\'à 8 vies et améliorer beaucoup plus. Les occasions de donner des organes sont rares en raison des circonstances particulières qui doivent être présentes. Toutefois, le don de tissus est possible pour la plus des cas. Après que tous les efforts de sauvetage ont échoué et que la mort a été déclarée à l\'hôpital, il y a une petite période de temps pour les greffes. Avec le consentement donné, des tests sont effectués pour confirmer la pertinence médicale des organes et des tissus, et pour trouver les meilleurs appariements pour les patients en attente de transplantation.Le Réseau Trillium pour le don de vie collabore étroitement avec une équipe de professionnels de la santé pour soutenir la famille endeuillée tout au long du processus. Le Réseau Trillium pour le don de vie collabore étroitement avec des professionnels de la santé pour soutenir la famille tout au long du processus.\r\nCela inclut une année dans le programme de soutien aux familles de donneurs, et la célébration annuelle de la vie en l\'honneur du don généreux et de l\'héritage de la vie. \r\nLe don donne à une famille quelque chose de positif dans une situation négative. C\'est une façon pour une famille d\'honorer les souhaits d\'être un donneur lorsque les décisions de consentement sont connus La possibilité d\'aider les personnes dans le besoin est réconfortante pour les familles.\r\nEn savoir plus sur le Réseau Trillium pour le don de vie giftoflife.on.ca \r\nInscrivez-vous aujourd\'hui beadonor.ca');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fr_tbl_contact`
--

DROP TABLE IF EXISTS `fr_tbl_contact`;
CREATE TABLE IF NOT EXISTS `fr_tbl_contact` (
  `contact_id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT,
  `contact_formtitle` varchar(70) NOT NULL,
  `contact_connecttitle` varchar(72) NOT NULL,
  `contact_hotline` varchar(70) NOT NULL,
  `contact_number` varchar(20) NOT NULL,
  PRIMARY KEY (`contact_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `fr_tbl_contact`
--

INSERT INTO `fr_tbl_contact` (`contact_id`, `contact_formtitle`, `contact_connecttitle`, `contact_hotline`, `contact_number`) VALUES
(1, 'comment pouvons nous aider', 'CONNECTE-TOI AVEC NOUS', 'NOTRE hotline', '1-888-888-888');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fr_tbl_facts`
--

DROP TABLE IF EXISTS `fr_tbl_facts`;
CREATE TABLE IF NOT EXISTS `fr_tbl_facts` (
  `fact_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT,
  `fact_first` varchar(60) NOT NULL,
  `fatc_second` varchar(60) NOT NULL,
  `fact_third` varchar(80) NOT NULL,
  `fact_icon` varchar(50) NOT NULL,
  PRIMARY KEY (`fact_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `fr_tbl_facts`
--

INSERT INTO `fr_tbl_facts` (`fact_id`, `fact_first`, `fatc_second`, `fact_third`, `fact_icon`) VALUES
(1, 'un donneur peut', 'sauver jusqu\'à 8 vies', 'et améliorer la vie de 75 autres', 'icon_gift.png'),
(2, '1,527', ' patients', 'en attente d\'une greffe d\'organe', 'icon_puzzle.png'),
(3, '32%', 'ont été enregistrés', 'pour être des donneurs', 'icon_heart.png'),
(4, 'tous les âges', 'peuvent se qualifier', 'pour être un donneur', 'icon_martini.png'),
(5, 'les grandes religions', 'soutiennent', 'le don d’organes', 'icon_check');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fr_tbl_myth`
--

DROP TABLE IF EXISTS `fr_tbl_myth`;
CREATE TABLE IF NOT EXISTS `fr_tbl_myth` (
  `myth_id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT,
  `myth_title` varchar(200) NOT NULL,
  `myth_desc` text NOT NULL,
  PRIMARY KEY (`myth_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `fr_tbl_myth`
--

INSERT INTO `fr_tbl_myth` (`myth_id`, `myth_title`, `myth_desc`) VALUES
(1, 'MYTHE # 1 Je suis trop vieux pour être un donneur d\'organes et de tissus', 'L\'âge n\'est pas un obstacle - les gens qu\'on 80 ans ou plus sont des donneurs d\'organes et de tissus\r\n\r\nLa majorité (78%) des Australiens âgés de 65 ans et plus sont prêts à donner des organes et des tissus, mais 37% supposent qu\'ils sont trop âgés pour être considérés pour le don d\'organes et de tissus.\r\n- Les personnes de 70 et 80 ans ont sauvé la vie d\'autrui grâce au don d\'organes et de tissus.\r\n- Chaque donneur potentiel est évalué individuellement. Il est possible que vous soyez en mesure de donner vos organes ou vos tissus.');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fr_tbl_promotional`
--

DROP TABLE IF EXISTS `fr_tbl_promotional`;
CREATE TABLE IF NOT EXISTS `fr_tbl_promotional` (
  `prom_id` smallint(5) UNSIGNED NOT NULL DEFAULT '0',
  `prom_image` varchar(80) NOT NULL,
  `prom_video` varchar(80) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fr_tbl_share`
--

DROP TABLE IF EXISTS `fr_tbl_share`;
CREATE TABLE IF NOT EXISTS `fr_tbl_share` (
  `share_id` mediumint(8) UNSIGNED NOT NULL DEFAULT '0',
  `share_name` varchar(100) NOT NULL,
  `share_age` tinyint(15) NOT NULL,
  `share_location` varchar(150) NOT NULL,
  `share_type` varchar(30) NOT NULL,
  `share_title` varchar(20) DEFAULT NULL,
  `share_story` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fr_tbl_titles`
--

DROP TABLE IF EXISTS `fr_tbl_titles`;
CREATE TABLE IF NOT EXISTS `fr_tbl_titles` (
  `titles_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `titles_name` varchar(50) NOT NULL,
  `titles_pagetitle` varchar(120) DEFAULT NULL,
  PRIMARY KEY (`titles_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `fr_tbl_titles`
--

INSERT INTO `fr_tbl_titles` (`titles_id`, `titles_name`, `titles_pagetitle`) VALUES
(1, 'À PROPOS', NULL),
(2, 'HISTOIRES', NULL),
(3, 'PARTICIPER', 'partagez votre souhait d\'être un donateur. Parlez à votre famille aujourd\'hui'),
(4, 'CONTACTEUR', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fr_tbl_wish`
--

DROP TABLE IF EXISTS `fr_tbl_wish`;
CREATE TABLE IF NOT EXISTS `fr_tbl_wish` (
  `wish_id` mediumint(8) UNSIGNED NOT NULL DEFAULT '0',
  `wish_email` varchar(80) NOT NULL,
  `wish_title` varchar(50) NOT NULL,
  `wish_message` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbl_users`
--

DROP TABLE IF EXISTS `tbl_users`;
CREATE TABLE IF NOT EXISTS `tbl_users` (
  `user_id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_fname` varchar(250) NOT NULL,
  `user_name` varchar(250) NOT NULL,
  `user_pass` varchar(250) NOT NULL,
  `user_email` varchar(250) NOT NULL,
  `user_date` varchar(70) DEFAULT NULL,
  `user_ip` varchar(50) NOT NULL DEFAULT 'no',
  `user_attempts` tinyint(4) NOT NULL,
  `user_lvl` varchar(50) DEFAULT NULL,
  `user_log` smallint(6) DEFAULT NULL,
  `user_newt` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tbl_users`
--

INSERT INTO `tbl_users` (`user_id`, `user_fname`, `user_name`, `user_pass`, `user_email`, `user_date`, `user_ip`, `user_attempts`, `user_lvl`, `user_log`, `user_newt`) VALUES
(1, 'Metis', 'metis', 'lol', 'metis@passislol', NULL, 'no', 0, NULL, NULL, '2018-03-27 20:18:33'),
(2, 'Mark', 'mark', 'LM9K::084b2003b29a91900719fd269667d576', 'mark@passiswow', '2018-03-27 20:29:52', '::1', 0, NULL, 2, '2018-03-27 20:20:30'),
(3, 'Joseph', 'jose', 'BVHy::797ed6ca5925b774134e85d34b40e79a', 'jose@passisyou', NULL, 'no', 0, NULL, NULL, '2018-03-27 20:38:47'),
(4, 'Romero', 'rom', 'xWd3::dc412e7a1bc5acc2c7de7d599f04c6f6', 'rome@passislol', NULL, 'no', 0, NULL, NULL, '2018-03-27 20:53:50'),
(5, 'Martina Lopez', 'martina', 'NpR+::241e0af6af74d8299bf6f173fe734663', 'mart@passisgol', '2018-03-27 20:56:26', '::1', 0, NULL, 1, '2018-03-27 20:55:33');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
