-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Mar 04, 2018 at 12:26 AM
-- Server version: 5.7.19
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `prueba`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

DROP TABLE IF EXISTS `tbl_user`;
CREATE TABLE IF NOT EXISTS `tbl_user` (
  `user_id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_fname` varchar(250) NOT NULL,
  `user_name` varchar(250) NOT NULL,
  `user_pass` varchar(250) NOT NULL,
  `user_email` varchar(250) NOT NULL,
  `user_date` varchar(70) DEFAULT NULL,
  `user_ip` varchar(50) NOT NULL DEFAULT 'no',
  `user_attempts` tinyint(4) NOT NULL,
  `user_lvl` varchar(50) DEFAULT NULL,
  `user_log` smallint(6) DEFAULT NULL,
  `user_newt` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`user_id`, `user_fname`, `user_name`, `user_pass`, `user_email`, `user_date`, `user_ip`, `user_attempts`, `user_lvl`, `user_log`, `user_newt`) VALUES
(4, 'hola', 'qtal', 'comowas', 'yolo@kavida', '2018-02-22 01:07:13', '::1', 0, '0', NULL, NULL),
(6, 'hola', 'user', 'yes', 'yelel@lek', NULL, 'no', 0, '2', NULL, NULL),
(19, 'hk', 'hyu', 'RNiRvA7q', 'hl', NULL, 'no', 0, '2', NULL, NULL),
(20, 'lol', 'wow', '', 'gol@gmail', NULL, 'no', 0, '2', NULL, NULL),
(21, 'gyul', 'h', '', 'hjuk', NULL, 'no', 0, '2', NULL, NULL),
(23, 'hilil', 'hil', '9+EU::7abef9d45705c3055a902fffe60ef1bc', 'lol@lol', '2018-03-01 00:01:06', '::1', 0, '1', 2, NULL),
(24, 'Camila', 'kmi', 'yolo', 'yolo', '2018-02-28 10:41:00', '::1', 0, '2', NULL, NULL),
(25, 'hola', 'fal', 'Ieh8::f7447f4965dc2994aa77a1b6672f1967', 'lol@fal', '2018-03-01 10:29:35', '::1', 0, '2', 1, NULL),
(26, 'Prueba', 'pro', 'O3fZ::15ddd86cbd7b23d824692d424fc51724', 'pol@pro', '2018-03-01 00:00:17', '::1', 0, '2', 4, NULL),
(27, 'Prueba2', 'pro2', '0Vgc::fb297ec54ea05a6c52c78b65bd886b36', 'yolo@pro2', '2018-02-28 23:53:22', '::1', 0, '2', 1, NULL),
(28, 'Prueba3', 'pro3', 'lefI::222da3f335e9c363b5a8ef1c2e641935', 'lol@pro3', '2018-02-28 23:56:22', '::1', 0, '2', 1, NULL),
(29, 'hola2', 'gol', 'aWs8::04e14cde21fc7207add3c1464ca2c452', 'lol', '2018-02-28 23:58:57', '::1', 0, '2', 3, NULL),
(30, 'le try', 'leroy', '3VB2::4efa813643d1d138cbff055afaa32177', 'opo@leroy', '2018-03-01 10:59:13', '::1', 0, '1', 2, '2018-03-01 10:30:18');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
