$(document).foundation();


var section = $('li');

function toggleAccordion() {
  section.removeClass('active');
  $(this).addClass('active');
}

function openTab(element) {
	section.removeClass('active');
	
	$("#"+element).addClass('active');
	$(".stories-section").fadeIn();
	$(".section--storiespreview").fadeOut();

	$('html, body').animate({
        scrollTop: $(".section--storiespreview").offset().top
    }, 2000);
}




section.on('click', toggleAccordion);


