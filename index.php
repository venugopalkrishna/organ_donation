
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Organ Donation</title>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link rel="stylesheet" type="text/css" href="css/foundation.min.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body>

	<div class="hero hero--language">
		<div class="call_to_action">
			<div class="container-button">
				<a href="homeen.php">
					<button>
						ENGLISH
					</button>
				</a>
				<a href="housefr.php">
					<button>
						FRANCAIS
					</button>
				</a>
			</div>
		</div>
	</div>

	<script src="js/vendor/jquery.js"></script>
    <script src="js/vendor/what-input.js"></script>
    <script src="js/vendor/foundation.js"></script>
    <script src="js/app.js"></script>

</body>
</html>
