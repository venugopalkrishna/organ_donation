<?php
require_once('admin/phpscripts/config.php');
$lang = "fr";
if(isset($_POST['submit'])) {
	$name = $_POST['name'];
	$age = $_POST['age'];
	$location = $_POST['location'];
	$type = $_POST['type'];
	$title = $_POST['title'];
	$message = $_POST['message'];
	// $result = ;
}
if(isset($_POST['contact'])) {
	$name = $_POST['name'];
	$email = $_POST['email'];
	$message = $_POST['message'];
	$result = submitMessage($name, $email, $message);
}
 ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Organ Donation</title>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link rel="stylesheet" type="text/css" href="css/foundation.min.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-114989043-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-114989043-1');
</script>

</head>
<body>
	<header>
 	 <div class="title-bar topbar-center-logo-mobile" data-responsive-toggle="topbar-center-logo" data-hide-for="medium">
 			 <div class="title-bar-left">
 				 <button class="menu-icon" type="button" data-toggle="topbar-center-logo"></button>
 				 <img src="img/search_bar.png" alt="">
 			 </div>

 		 <div class="title-bar-center">
 			 <div class="title-bar-title"><img src="img/logo_temp.png" alt="" /></div>
 		 </div>

 		 <div class="title-bar-right">
			 <a href="userlogin.php"><img src="img/sign_In.png" alt="sign in"></a><
 		 </div>


 	 </div>
 	 <!-- /mobile nav bar -->

 	 <!-- medium and larger nav bar -->
 	 <div class="top-bar topbar-center-logo" id="topbar-center-logo">
 			 <div class="top-bar-left">
 				 <ul class="menu vertical medium-horizontal" data-magellan>
 					 <li class="icon__item"><img src="img/search_bar.png" alt=""></li>
 					 <li><a href="#about">&Agrave; PROPOS</a></li>
 					 <li><a href="#stories">HISTOIRES</a></li>
 				 </ul>
 			 </div>

 			 <div class="top-bar-center">
 				 <img src="img/logo_temp.png" alt="">
 			 </div>

 		 <div class="top-bar-right">
 			 <ul class="menu vertical medium-horizontal">
 				 <li><a href="#share">PARTICIPER</a></li>
 				 <li><a href="#contact">CONTACTEUR</a></li>
 				 <li class="icon__item"><a href="homeen.php">EN </a>
 				 </li>
				 <li class="hide-for-small-only">
 					 <div class="icon-signin"><a href="userlogin.php"><img src="img/sign_In.png" alt="sign in"></a></div>
 				 </li>
 			 </ul>
 		 </div>
 	 </div>
  </header>
	<div class="hero vid">
		<div class="hero__title">
			<h2 id="heroText1"></h2>
			<h1 id="heroText2"></h1>
		</div>
		<div class="call_to_action">
			<p>#celebr8lives</p>
			<a href="https://www.beadonor.ca/"><button>
				Inscrivez-vous dès Maintenant
				<span>beadonor.ca</span>
			</button></a>
		</div>
	</div>

	<!-- ABOUT -->
	<div class="section" id="about" data-magellan-target="about">
		<?php
		$id = 1;
		include "admin/includes/titles.php";
		?>

		<!-- About Slider Mobile-->
		<div class="row about__mobile">
		  <div class="columns">
		    <div class="orbit" role="region" aria-label="Favorite Text Ever" data-orbit>
		      <ul class="orbit-container">
		        <button class="orbit-previous" aria-label="previous">
		        	<img src="img/left.png" alt="left">
		        </button>

		        <button class="orbit-next" aria-label="next">
		        	<img src="img/right.png" alt="right">
		        </button>

						<?php include "admin/includes/facts.php"; ?>

		      </ul>
		    </div>
		  </div>
		</div>
		<!-- About slider Mobile -->

		<!-- About Slider Desktop -->
		<div class="data_container about__desktop grid-x align-center">
			<?php include "admin/includes/factssmall.php"; ?>
		</div>
		<!-- About Slider Desktop -->

		<div class="section__content section__content--about">
			<!-- myth pop up -->
			<?php
			$tbl = "_tbl_myth";
			// $lang = "en";
			$contents = generalCnt($tbl, $lang);

			while ($row = mysqli_fetch_array($contents)) {
				echo "<div class=\"mythpop\">
					<a class=\"closeBio\"><p class=\"hidden\">X</p></a>
					<h3 class=\"title\">{$row['myth_title']}</h3>
					<div class=\"grid-x\">
						<p class=\"description2\">{$row['myth_desc']}</p>
					</div>
				</div>";
			}
			 ?>
	<!-- myth pop up end -->
		<h2 class="title abn small-center">
			De quoi ai-je besoin pour m'inscrire?
		</h2>

		<div class="row about__mobile">
		  <div class="columns">
		    <div class="orbit" role="region" aria-label="Favorite Text Ever" data-orbit>
		      <ul class="orbit-container">
		        <button class="orbit-previous" aria-label="previous">
		        	<img src="img/left.png" alt="">
		        </button>

		        <button class="orbit-next" aria-label="next">
		        	<img src="img/right.png" alt="">
		        </button>

						<?php include "admin/includes/sabout.php"; ?>

		      </ul>
		    </div>
		  </div>
		</div>

		<div class="grid-x about__desktop">
			<?php include "admin/includes/about.php" ?>
			<button class="button_left">MYTH BUSTINGS</button>
		</div>
	</div>
	<div class="medium-12">
		<div class="video">
			<video src="video/Final.mp4" controls>

			</video>
	</div>
	</div>
</div>

	<!-- STORIES -->
	<div class="section section--storiespreview" id="section--storiespreview">
		<?php
		$id = 2;
		include "admin/includes/titles.php";
		?>

		<div class="section__content section__content--stories">
			<div class="grid-x">
				<?php include "admin/includes/storiesblock.php"; ?>
			</div>

		</div>

	</div>


	<!-- NEW STORIES-->
	<div class="section stories-section" id="share" data-magellan-target="share">
		<?php
		$id = 2;
		include "admin/includes/titles.php";
		?>

		<div class="section__content">

			<ul class="accordion">
			  <li class="active" id="accordion-one" >
			    <div class="section-title">
			      <h2>WITH THE TRANSPLANT</h2>
			    </div>

			    <div class="section-content">
			    	<div>
						<div class="orbit"  aria-label="Favorite Text Ever" data-orbit>
						    <div class="orbit-wrapper">
									<?php include "admin/includes/stories.php"; ?>
						    </div>

					      	<nav class="orbit-bullets">
						        <button class="is-active" data-slide="0"><span class="show-for-sr">First slide details.</span><span class="show-for-sr">Current Slide</span></button>
					        	<button data-slide="1"><span class="show-for-sr">Second slide details.</span></button>
					      	</nav>
					    </div>
			    	</div>
			  </li>

			  <li id="accordion-two">
			    <div class="section-title">
			      <h2>I CHOSE TO BE A DONOR</h2>
			    </div>

			    <div class="section-content">
					<div class="orbit orbit-custom" role="region" aria-label="Favorite Text Ever" data-orbit>
						    <div class="orbit-wrapper">
									<?php include "admin/includes/stories_img.php"; ?>
						    </div>

					      	<nav class="orbit-bullets">
						        <button class="is-active" data-slide="0"><span class="show-for-sr">First slide details.</span><span class="show-for-sr">Current Slide</span></button>
					        	<button data-slide="1"><span class="show-for-sr">Second slide details.</span></button>
					      	</nav>
					    </div>


			  </li>

			  <li id="accordion-three">
			    <div class="section-title">
			      <h2>WHAT'S YOUR STORY?</h2>
			    </div>
			    <div class="section-content">
						<?php
						$tbl = "_tbl_storieshare";
						// $lang = "en";
						$contents = generalCnt($tbl, $lang);
						while ($row = mysqli_fetch_array($contents)) {
						echo "
			    	<div class=\"container-content cell medium-9\">
			    		<h3 class=\"title\">{$row['ss_header']}</h3>
						<form class=\"grid-x\" action=\"home.php\" method=\"post\">
							<div class=\"cell small-12 medium-11\">
								<div class=\"grid-x\">
									<input class=\"medium-6\" type=\"text\" name=\"name\" value=\"\" placeholder=\"{$row['ss_name']}\">
									<input class=\"medium-2\" type=\"text\" name=\"age\" value=\"\" placeholder=\"{$row['ss_age']}\">
									<input class=\"medium-3\" type=\"text\" name=\"location\" value=\"\" placeholder=\"{$row['ss_location']}\">
									<div class=\"small-12 medium-7\">
										<div class=\"grid-x\">
											<input class=\"small-1 medium-1\" type=\"radio\" name=\"name\" value=\"Anonimous\">
											<p class=\"small-10 medium-10\">{$row['ss_anon']}</p>
										</div>
									</div>
									<div class=\"small-6 medium-2\">
										<div class=\"grid-x\">
											<input class=\"small-1 medium-1\" type=\"radio\" name=\"type\" value=\"receiver\">
											<p class=\"small-9 medium-9\">{$row['ss_rec']}</p>
										</div>
									</div>
									<div class=\"small-6 large-3\">
										<div class=\"grid-x\">
											<input class=\"small-2 medium-2\" type=\"radio\" name=\"type\" value=\"register\">
											<p class=\"small-9 medium-9\">{$row['ss_reg']}</p>
										</div>
									</div>
									<input class=\"medium-6\" type=\"text\" name=\"title\" value=\"\" placeholder=\"{$row['ss_title']}\">
									<div class=\"small-12 medium-5\">
										<div class=\"grid-x\">
											<input class=\"small-3 medium-1\" type=\"radio\" name=\"type\" value=\"family\">
											<p class=\"small-8 medium-10\">{$row['ss_fam']}</p>
										</div>
									</div>
								</div>
							</div>
							<input class=\"small-12 medium-10\" type=\"text\" name=\"message\" value=\"\" placeholder=\"{$row['ss_message']}\">
							<input class=\"small-6 medium-2 submit\" type=\"submit\" name=\"submit\" value=\"{$row['ss_button']}\">
						</form>
			    	</div>";
						}
						?>
			    </div>
			  </li>
			</ul>

		</div>
	</div>


	<!-- SHARE -->
	<div class="section" id="share" data-magellan-target="share">
		<?php
		$id = "3";
		include "admin/includes/titles.php";
		?>

		<div class="section__content section__content--share">
			<div class="share-container">
				<?php echo "<h2 class=\"title\">partagez votre souhait d'être un donateur. Parlez à votre famille aujourd'hui</h2>"; ?>
				<div class="grid-x">
					<div class="cell medium-5">
						<form action="homeen.php" method="post">
							<input type="email" name="email" placeholder="Votre E-mail">
							<input type="email" name="emails" placeholder="Récepteur du E-mail">
							<input type="text" name="titles" placeholder="Titre">
							<textarea name="message" id="" cols="30" rows="6" placeholder="Votre Message"></textarea>
						<br>
						<h5>Partager via les médias sociaux :</h5>
						<div class="grid-x medium-12 social">
							<div class="button button--facebook cell medium-4">facebook</div>
							<div class="button button--messenger cell medium-4">messenger</div>
							<div class="button button--twitter cell medium-4">twitter</div>
						</div>
						<input class="medium-6 button" type="submit" name="submit" value="ENVOYER">
					</form>

					</div>

					<div class="medium-5 medium-offset-2	">
						<img src="img/share_card.jpg" alt="share">
						<label for="">
							<input type="checkbox">
							choisi la carte avec votre message
						</label>

						<div class="grid-x card-options">
							<span class="cell medium-6">or <br>
								<a href="#">Personnaliser la carte</a></span>

							<span class="cell medium-6">or <br>
								<a href="#">Importer une carte</a></span><br>
								<div style="float:right;" class="">
									<span>Besoin de conseil</span>

									<div class="call">
										<div class="button button--twitter cell medium-4">hotline</div>
										<p>Téléphoner à notre bureau <br>1-999-999-9999</p>
									</div>
								</div>

						</div>
					</div>

				</div>

			</div>
		</div>

	</div>
	<br>
	<!-- CONTACT -->
	<div class="section" id="contact" data-magellan-target="contact">
		<?php
		$id = "4";
		include "admin/includes/titles.php";
		?>

		<div class="section__content section__content--contact">
			<div class="contact-container">

				<div class="grid-x">
					<div class="cell medium-5">
						<h2 class="title">comment pouvons nous aider</h2>
						<form class="" action="housefr.php" method="post">
							<input type="text" name="name" placeholder="Nom">
							<input type="text" name="email" placeholder="Votre E-mail">
							<textarea type="text" name="message" id="" cols="30" rows="6" placeholder="Votre Message"></textarea>
							<br>

							<input class="submit" type="submit" name="contact" value="ENVOYER">
						</form>
					</div>

					<div class="medium-5 medium-offset-2	">
						<h2 class="title">comment pouvons nous aider</h2>

						<div class="grid-x">
							<div class="square">
								<a href="https://www.facebook.com/"><img src="img/fb.png" alt="facebook"></a>
							</div>
							<div class="square">
								<a href="https://twitter.com/"><img src="img/twt.png" alt="twitter"></a>
							</div>
							<div class="square">
								<a href="https://www.youtube.com/"><img src="img/youtube.png" alt="youtube"></a>
							</div>
							<div class="square">
								<a href="https://www.instagram.com/"><img src="img/insta.png" alt="instagram"></a>
							</div>
							<div class="square">
								<a href="https://www.pinterest.com/"><img src="img/pin.png" alt="pinteres"></a>
							</div>

						</div>

						<h2 class="title">NOTRE HOTLINE</h2>
						<h2 class="title">1-888-888-888</h2>

					</div>

				</div>

			</div>
		</div>

		<div class="contact-footer">
			<div class="grid-x">
				<div class="medium-8">
					<div class="contact-footer__img">
						<img src="img/s_logo.png" alt="celebrate lives">
					</div>
					<span>#celebr8lives</span>
				</div>
				<div class="cell medium-4 partners">
					<h5>NOS PARTENAIRES</h5>
					<div class="grid-x">
						<div class="medium-4 partner-block"><img src="img/beadonor.png" alt="beadonor.ca"></div>
						<div class="medium-4 partner-block"><img src="img/sercan.png" alt="service canada"></div>
						<div class="medium-4 partner-block"><img src="img/triflium.png" alt="triflium"></div>
					</div>
				</div>
			</div>

		</div>

	</div>


	<footer>
		&copy; Copyrights 2018 Collège Fanshawe. Tous les droits sont réservés; Termes et conditions. Confidentialité et sécurité. Carte du site
	</footer>



	<script src="js/vendor/jquery.js"></script>
    <script src="js/vendor/what-input.js"></script>
    <script src="js/vendor/foundation.js"></script>
    <script src="js/app.js"></script>
	<script src="js/principal.js"></script>
	<!-- <script src="js/slider.js"></script> -->


</body>
</html>
