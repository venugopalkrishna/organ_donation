# Organ Donation Folder
This folder contains the files that are to be in the Organ Donation Project done to encourage people to donate their organs after they have passed away in Ontario.

## How to make it Work in your Computer
To make use of the folders and documents you will have to download Github from this link:

* http://rogerdudler.github.io/git-guide/

then go to "Set Up" and download the file that is for your operative system.

After the Github setup, go to the ledc Overview and copy the link that is provided. From there, open Terminal or Command Prompt and go to your desired empty folder and type

```
git clone <<insert link here>>
```

This will automatically download the files from BitBucket and save them in your system.

Now, if you were to start editing the content you will need to get Sass in your computer to make sure that all changes made in CSS are saved, otherwise the edits you make will not be saved.

## Pre requisites
* Its imperative for you to have Github installed in your computer to download the files
* If you were to edit the CSS in the folder, then make sure you install Ruby (if you have Windows or Linux) and then install Sass.

## Installing
To install Sass there will be 2 different routes to take depending on the operative system you are using
**Mac**
Go to the terminal and type this:

```
sudo gem install sass
```

After that, press enter and check if Sass has been installed right typing:

```
sass -v
```

It should return you with a message that reads something similar to:

```
Sass 3.5.1.
```

Now navigate to the organ_donation folder and type

```
sass --watch sass:css
```

And done you are good to go.

**Windows and Linux: Ruby Installation**
For the Windows system and even Linux you will need to install Ruby in your computer, following these steps
* Go to: https://rubyinstaller.org/downloads/ and download any of the 2.4.X (X stands for any other number) files.

* Follow the directions given by the webpage

**Sass on Windows**
* Install Ruby first and then type:

```
gem install sass
```

* Then follow by typing

```
sass -v
```

* Now navigate to the organ_donation folder and type

```
sass --watch sass:css
```

And done you are good to go.


**Sass on Linux**
* Always make sure you have Ruby installed, but after that type

```
sudo gem install sass --no-user-install
```

* Make sure Sass was installed asking for the version typing

```
Sass -v
```

* Now navigate to the organ_donation folder and type

```
sass --watch sass:css
```

And done you are good to go.


##Built with:
* Sass - A quicker way to write CSS
* Foundation - The web framework used
* Greensock - The JavaScript animation library
